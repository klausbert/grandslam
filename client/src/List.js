import React from 'react';

const List = ({ grid, dateTime, spaces, slots, types }) => {
	if (! grid)
		return (<div />);

	if (dateTime==='')
		return (<div />);
	
	const currentTime = dateTime.substr(11, 5)
	
	const groups = [...new Set(spaces.map( m => m.group )) ].sort()
	
	return (
		<div id="list">
			{ groups.map( g => (
				<div key={g} className="row">
					<div className="page-header col-xs-12"><h2>Sector {g} a las { currentTime }</h2></div>
					{
						spaces.filter( f => f.group===g )
						.map( c => (
							<div className="col-xs-4" key={c.id}>								
								<div className="well">
									<h3>Cancha {c.id}</h3>
									{	slots
										.map( r => grid[r.id] || [] )
										.map( row => row[c.id] || [] )
										.filter( f => f.type_id > 0 )
										.map((i, j) => (
											<div className="row" key={j}>
												<div className="col-xs-2">{ i.slot_id }</div>
												<div className="col-xs-2">{ types.find( t => t.id===i.type_id ).name }</div>
												<div className="col-xs-8">
													{/* { i.players && i.players.map( p => <span key={p.id} className="badge">{ p.name }</span> )} */}
													{/* { i.label==='D' ? JSON.stringify(i) : null } */}
												{	i.label!=='S' && i.label!=='D' ? null :
													i.players && i.players.map( p => <span key={p.id} className="badge">{ p.name }</span> )
												}
												</div>
											</div>
										))
									}
								</div>
							</div>
						))
					}
					<hr/>
				</div>
			))}
		</div>
	)
}
export default List 
