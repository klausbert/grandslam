import React, { createContext, useState, useContext } from 'react'

import { AppContext } from './AppContext'
import useTimeout from './useTimeout'


export const TimerContext = createContext()
export const TimerContextProvider = ({ children }) => {
    const { slots } = useContext(AppContext)
    const [dateTime, setDateTime] = useState(new Date().toJSON())
    
    const currentTime = () => dateTime.substr(11, 5)
    const currentSlot = () => {
        const slot = slots.filter( f => f.name < currentTime()).pop()	//	17:30 17:35

        return slot ? slot.id : slots.pop().id
    }
    useTimeout(() => setDateTime(new Date().toJSON()), 60000)

    return (
    <TimerContext.Provider value={{ dateTime, currentSlot }}>
        { children }
    </TimerContext.Provider>
    )
}
export const useStateValue = () => useContext(TimerContext)
