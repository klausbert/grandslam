import axios from 'axios'

const api = axios.create({
    baseURL: ''
})


export async function slots() {
    const data = await api
    .get(`/slots`)
    .then( done => done.data )
    .catch( err => ({ error: err.response }))
    
    return data
}
export async function spaces() {
    const data = await api
    .get(`/spaces`)
    .then( done => done.data )
    .catch( err => ({ error: err.response }))
    
    return data
}
export async function types() {
    const data = await api
    .get(`/types`)
    .then( done => done.data )
    .catch( err => ({ error: err.response }))
    
    return data
}
export async function timer() {
    return { dateTime: new Date().toJSON().replace('T', ' ') }
}


export const places = {
    get: async (date) => {
        const data = await api
        .get(`/places?date=${date}`)
        .then( done => done.data )
        .catch( err => ({ error: err.response }))
            
        return data
    },
    post: async (cell) => {
        const data = await api
        .post(`/places`, cell)
        .then( done => done.data )
        .catch( err => ({ error: err.response }))
            
        return data
    },
    put: async (id, cell) => {
        const data = await api
        .put(`/places/${id}`, cell)
        .then( done => done.data )
        .catch( err => ({ error: err.response }))
            
        return data
    },
    delete: async (id) => {
        const data = await api
        .delete(`/places/${id}`)
        .then( done => done.data )
        .catch( err => ({ error: err.response }))
            
        return data
    },
    copy: async (source, target) => {
        const data = await api
        .get(`/places/copy?source=${source}&target=${target}`)
        .then( done => done.data )
        .catch( err => ({ error: err.response }))
            
        return data
    }
}


export async function members(query) {
    const data = await api
    .get(`/members?name=${query}`)
    .then( done => done.data )
    .catch( err => ({ error: err.response }))
    
    return data
}
export async function member(id, date) {
    const data = await api
    .get(`/members/${id}/status/${date}`)
    .then( done => done.data )
    .catch( err => ({ error: err.response }))
    
    return data
}


export async function login({ user, pass }) {
    const data = await api
    .post(`/users`, { user, pass })  //  session
    .then( done => done.data )
    .then( data => Array.isArray(data) && data[0])
    .catch( err => ({ error: err.response }))

    return data 
}
