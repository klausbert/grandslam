import React from 'react'
import moment from 'moment'
import 'moment/locale/es'

import { useStateValue } from './TimerContext'


const Timer = ({ className }) => {
	const { dateTime } = useStateValue()
	if (dateTime===undefined)
		return null;
	
	const date = moment(new Date(dateTime)).format("dddd, DD [de] MMMM [de] YYYY, HH:mm")
		.split(" ").map( m => { if (m.length > 2) m = m.substr(0, 1).toUpperCase() + m.substr(1); return m }).join(" ")
	
	return (
		<form className={ "navbar-form "+ className }>
			<div className="form-group">
				<div className="form-control-static lead" style={{ color: 'WHITE', fontWeight: 'bold' }}>{ date }</div>
			</div>
		</form>
	)
}
export default Timer