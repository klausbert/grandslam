// Not implemented yet - go to AppContext instead
import React, { createContext, useState, useContext } from 'react'
import { login } from './api'


export const StateContext = createContext()
export const StateProvider = ({ children }) => {
    const [user, setUser] = useState(null)

    const getUser = async ({ user, pass }) => {
        const data = await login({ user, pass })
        console.log('Login', data)
        setUser(data)
    }
    return (
    <StateContext.Provider value={{ user, getUser }}>
        { children }
    </StateContext.Provider>
    )
}
export const useStateValue = () => useContext(StateContext)
