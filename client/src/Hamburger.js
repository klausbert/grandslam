import React from 'react'
import { ButtonToolbar, Dropdown, MenuItem } from 'react-bootstrap'

import * as api from './api'


const handleSelect = () => {
	const target = (new Date()).toJSON().slice(0, 10)
	const source = prompt('Copiar reservas del dia', target)
	
	if (source && source!==target) {
		api.places.copy(source, target).then( data => console.log('Copy', data))
	}
}

const Hamburger = ({ className, user, onUpdate }) => {

	if (! user) return <div />

	const handleCalendar = () => onUpdate({ showCalendar: true })
	
	return (
		<ButtonToolbar className={ "navbar-form "+ className }>
			<Dropdown pullRight id="dropdown-custom-1">
				<Dropdown.Toggle>
					<span className="fa fa-bars"></span>&nbsp;
				</Dropdown.Toggle>
				
				<Dropdown.Menu>
					<MenuItem onSelect={ handleSelect }> Copiar </MenuItem>
					<MenuItem onSelect={ handleCalendar }> Navegar </MenuItem>
					<MenuItem divider />
					<MenuItem>{ process.env.REACT_APP_VERSION }</MenuItem>
					<MenuItem onSelect={() => onUpdate({ dateTime: '', user: null })}> Salir </MenuItem>
				</Dropdown.Menu>
			</Dropdown>
		</ButtonToolbar>
	)
}
export default Hamburger
