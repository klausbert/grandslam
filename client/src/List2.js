import React from 'react';

const List = ({ grid, dateTime, spaces, slots, types }) => {

	const list = [].concat(
		...Object.keys(grid).map( slot => 
			Object.keys(grid[slot]).map( space => 
				grid[slot][space]
			).filter( f => f.players && f.label!=='X' )
		)
	)

	const players = [].concat(
		...list.map( place => 
			place.players.map( player => 
				({...player, slot_id: place.slot_id, space_id: place.space_id, label: place.label })
			)
		)
	).sort((a, b) => { return (a.name < b.name ? -1 : a.name===b.name ? 0 : 1) })

	return (
		<div id="list">
			<div className="row">
				<div className="col-xs-4">Socio</div>
				<div className="col-xs-1">Carnet</div>
				<div className="col-xs-4">Validación</div>
				<div className="col-xs-1 text-right">Hora</div>
				<div className="col-xs-1 text-right">Cancha</div>
				<div className="col-xs-1 text-right">Juego</div>				
			</div>
		{
			players.map((player, key) => 
				<div key={ key } className="row">
					<div className="col-xs-4">{ player.name }</div>
					<div className="col-xs-1">{ player.id }</div>
					<div className="col-xs-4">{ player.message }</div>
					<div className="col-xs-1 text-right">{ player.slot_id }</div>
					<div className="col-xs-1 text-right">{ player.space_id }</div>
					<div className="col-xs-1 text-right">{ player.label }</div>
				</div>
			)
		}
		</div>
	)
}
export default List
