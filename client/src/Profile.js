import React from 'react';

const Profile = (props) => {
	if (! props.user)
		return (<div />);
	
	return (
		<form className={ "navbar-form "+ props.className }>
			<div className="input-group">
				<input className="form-control" type="text" readOnly value={ props.user.account } />
				
				<div className="input-group-btn">
					<button className="btn" onClick={() => props.onUpdate({ dateTime: '', user: null })}>
						<span className="fa fa-sign-out"></span>
					</button>
				</div>
			</div>
		</form>
	)
}
export default Profile
