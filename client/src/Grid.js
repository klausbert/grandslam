import React, { useContext } from 'react'

import { GridContext } from './GridContext'

import Form from './Form'
import GridView from './GridView'
import ListWrapper from './ListWrapper'


export default function Grid() {
	const props = useContext(GridContext)

	console.log('Grid.render', props)

	if (props.dateTime==='') return <div />
	
	if (props.form.show) {
		return <Form {...props } onUpdate={ props.handleForm } />
	}
	else if (props.mode==='grid') {
		return <GridView {...props } grid={ props.grid } onClick={ props.handleClick } />
	}
	else {
		return <ListWrapper {...props } grid={ props.grid } />
	}
}
