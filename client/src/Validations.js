import React from 'react';

const Validations = (props) => {
	
	const players = props.players.map( m => {
		m.check = false;
		if (! m.check && ! m.enabled)
			m.check = true;
		if (! m.check && m.prior && m.prior.slot_id)
			m.check = true;
		return m
	})
	console.log(players);
	
	return (
		<div className="alert alert-warning">
		{
			players.map((m, i) => {
				return (
					<div className="row" key={i}>
						<div className="col-xs-6">{ m.name }</div>
						<div className="col-xs-3">{ m.message }</div>
						<div className="col-xs-2">{ m.prior && m.prior.slot_id ? m.prior.slot_id : null }</div>
						<div className="col-xs-1">{ m.check ? (<input type="checkbox" />) : "OK" }</div>
					</div> 
				)
			})
		}
		</div>
	)
}
export default Validations
