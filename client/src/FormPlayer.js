import React, { Component } from 'react'
import { asyncContainer, Typeahead } from 'react-bootstrap-typeahead'
import * as api from './api'

const AsyncTypeahead = asyncContainer(Typeahead);

export default class FormPlayer extends Component {

	state = { 
		player: this.props.players[this.props.index],
		options: [], 
	}
	componentWillReceiveProps(props) {
		this.setState({ player: props.players[props.index] })
	}
	render() {
		return (
			<form className="form-horizontal">
				<div className="form-group">
					<div className="col-xs-8">
						<AsyncTypeahead delay={400} labelKey="player" allowNew={false} disabled={ this.props.disabled } 
							options={ this.state.options } useCache={false} isLoading={false}
							placeholder={ this.state.player.id ? this.state.player.name +' - '+ this.state.player.id : `Jugador ${this.props.index + 1}` }
							onSearch={ this.handleSearch }
							onChange={ this.handleSelect }
						/>
					</div>
					
					<div className="col-xs-4 text-right">
						<label className="form-control-static">
							{ this.state.player.message }&nbsp;
							<span className={ this.state.player.enabled ? "fa fa-check" : "fa fa-flag" }></span>
						</label>
					</div>
				</div>
			</form>
		)
	}
	/*
	**	Typeahead
	*/
	handleSearch = (query) => {
		api.members(query).then( options => {
			console.log('options BEFORE filtering %O, players %O', options, this.props.players)
			options = options
			.filter( f => ! this.props.players.find( p => p.id===f.id ))
			.map( m => {
				m.player = m.name +' - '+ m.id
				return m
			})
			console.log('options AFTER filtering %O', options)
			this.setState({ options })
		})
	}
	handleSelect = (options) => {
		if (options && options.length) {
			const selectedPlayer = options[0]
			
			this.validateCode(selectedPlayer)
			this.setState({ options: [] })
		}
	}
	validateCode = async (player) => {
		const id = parseInt(player.id, 10)
		
		if (isNaN(id) || id < 10000 || id > 999999) {
			player = {...player, enabled: false, message: 'Fuera de rango' }
		} else {
			const res = await api.member(id, this.props.date)
			
			if (res && res.Habilitado)	//	por que no habria respuesta?
				player = {...player, enabled:(res.Habilitado!=='N'), message:(res.Habilitado!=='N' ? 'ok' : res.Mensaje) || '(validar)' }
			else
				player = {...player, enabled: true, message: 'ok' }
			
			if (res.prior) {
				const day = res.prior.date.substr(8, 2)
				const slot = (''+ res.prior.slot_id)
				player = {...player, enabled: false, message: `${day}, ${slot.substr(0, 2)}:${slot.substr(2, 2)}, cancha ${res.prior.space_id}` }
			}
			console.log('fetched player', player, res)
		}		
		player = {...player, index: this.props.index }
		console.log('validated player', player)
		this.setState({ player })
		
		this.props.onUpdate(player)
	}
}
