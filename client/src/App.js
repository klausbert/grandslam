import React, { useContext, Fragment } from 'react'
import { Router, Redirect } from '@reach/router'

import { AppContext } from './AppContext'
import { TimerContextProvider } from './TimerContext'

import Header from './Header'
import GridWrapper from './GridWrapper'


export default function App() {
	const todaysDate  = new Date().toJSON().slice(0, 10)

	const { user } = useContext(AppContext)
	// const props = useStateValue()

	return (
		<TimerContextProvider>
			<Fragment>
				<Router primary={ false }>
					<Header path=":date/:mode" />
					<Header path="/*" />
				</Router>
				
				<main className="container-fluid">
					<section id="main" role="main">
					{ 	user &&
						<Router>
							<GridWrapper path=":date/:mode" />

							<Redirect from="/*" to={`${todaysDate}/grid`} />
						</Router>
					}
					</section>
				</main>
			</Fragment>
		</TimerContextProvider>
	)
}
