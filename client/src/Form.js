import React, { useState, useEffect } from 'react';
import { Modal, Button } from 'react-bootstrap';
import * as api from './api'

import moment from 'moment';
import FormPlayer from './FormPlayer';

moment.locale('es');

export default function Form({ slots, currentSlotId, form, onUpdate }) {
	const item = form.item

	const [players, setPlayers] = useState([])
	const [exception, setException] = useState(item.exception || '')
	const [submit, setSubmit] = useState(false)
	
	const playerCount = item && item.type_id ? item.type_id * 2 : 0
	
	const editable = (item.slot_id >= currentSlotId)
	const killable = (editable && item.players && item.players.length >= 0)

	// render() {
	const date = moment(item.date +'T03:00:00Z').format("dddd, DD [de] MMMM [de] YYYY")
		.split(" ")
		.map( m => { if (m.length > 2) m = m.substr(0, 1).toUpperCase() + m.substr(1); return m })
		.join(" ")
	const time = slots.find( f => f.id===item.slot_id ).name
	
	const exceptions = players.reduce((r, c) => { return (c.enabled ? r - 1 : r)}, playerCount)
	
	// componentDidMount() {
	useEffect(() => {
		if (! item.type_id) {
			console.log('zás, no type id');
			return
		}		
		const formPlayers = (item.players || [])
		.map( m => ({...m, player: m.name +' - '+ m.id }))
		
		if (! item.id) {
			api.places.post(item).then( res => { 
				item.id = res.id 
			})
		}
		for (var j = formPlayers.length; j < playerCount; j++)
			formPlayers.push({ enabled: false })
		
		setPlayers(formPlayers)
		
		console.log('Es editable? %s', editable)

		return	//	or a function to unmount
	// }
	}, [])

	const handlePlayer = (player) => {
		console.log('Form.handlePlayer', players, player);
		
		let formPlayers = players
		formPlayers[player.index] = player
		
		if (players.filter( f => f.id===player.id ).length > 1) {
			player.enabled = false
			player.message = 'Duplicado'
		}
		const exceptions = formPlayers.reduce((r, c) => { return (c.id ? r - 1 : r)}, playerCount)
		
		setPlayers(players)
		setSubmit(! exceptions)
	}
	const handleInput = ({ target }) => setException(target.value)
	
	/*
	**	Close y Submit son parte del Form
	*/
	const handleClose = () => onUpdate({ show: false })	// no debe modificar
	
	const handleSubmit = () => {
		api.places.put(item.id, { exception, players })
		
		item.players = players
		onUpdate({ show: false, item: item })
	}
	const handleCancel = () => api.places.delete(item.id)
	.then( res => {
		console.log('Form.handleCancel DELETE response', res)
		
		onUpdate({ show: false, item: {...item, type_id: 0, players: [] }})
	})

	return (
		<Modal show={ form.show } onHide={ handleClose }>
			<Modal.Header closeButton>
				<Modal.Title>
					{ date } 
				<br /> 
					a las { time } en la Cancha { item.space_id }
				</Modal.Title>
			</Modal.Header>

			<Modal.Body>
				<p><span className="fa fa-id-card pull-right" style={{ cursor: 'pointer' }}></span></p>
				{
					players.map((_, i) =>
						<FormPlayer key={i} 
							players={ players } index={i} 
							date={ item.date } disabled={ ! editable } 
							onUpdate={ handlePlayer } 
						/>
					)
				}
				{
					players.length && exceptions
					?	<div className="alert alert-warning">
							{ exceptions } excepciones
							{
								players.reduce((r, c) => { return (c.id ? r + 1 : r)}, 0)===playerCount
								?	<input className="form-control" type="text" placeholder="Aclarar por qué se acepta la reserva con Excepciones" 
										value={ exception } onChange={ handleInput }
									/>
								:	null
							}
						</div>
					:	null
				}
				<div className="row">
					<div className="col-xs-6">
						<div className={ killable ? "text-left" : "hidden" }>
							<Button bsStyle={ editable ? "danger" : "info" } onClick={ handleCancel }>
							{
								editable ? "Cancelar Reserva" : "No se puede cancelar"
							}
							</Button>
						</div>
					</div>
					
					<div className="col-xs-6 text-right">
						<Button bsStyle={ editable ? (submit ? "success" : "default") : "info" }
							disabled={ ! submit } onClick={ handleSubmit }>
							{
								editable ? "Confirmar Reserva" : "No se puede modificar"
							}
						</Button>
					</div>
				</div>
			</Modal.Body>
		</Modal>
	)
	// }
}
