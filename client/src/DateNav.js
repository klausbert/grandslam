import React from 'react';
import { Modal } from 'react-bootstrap';
import InfiniteCalendar from 'react-infinite-calendar';
import 'react-infinite-calendar/styles.css'; // only needs to be imported once

const DateNav = ({ selectedDate, showCalendar, onUpdate }) => {
	const today = new Date();
	const lastWeek = new Date(today.getFullYear(), today.getMonth() - 1, today.getDate() - 7);
	
	const handleSelect = (dateObj) => {
		selectedDate = dateObj.toJSON().slice(0, 10);

		onUpdate({ selectedDate, showCalendar: false })	//	force Hide (close)
	}
	return (
		<Modal show={ showCalendar } onHide={() => onUpdate({ selectedDate, showCalendar: false })}>
			<Modal.Body>
				<InfiniteCalendar
					width={568}
					height={300}
					selected={today}
					XXXdisabledDays={[1,2,3,4,5]}
					XXXminDate={lastWeek}
					onSelect={ handleSelect }
				/>
			</Modal.Body>
		</Modal>
	)
}
export default DateNav
