import React from 'react';

import Cell from './Cell';

const GridView = ({ spaces, slots, currentSlotId, grid, onClick }) => {
	return (
		<table className="table" style={{ tableLayout: 'fixed' }}>
			<colgroup>
				<col width="100" />
			</colgroup>
			
			<thead>
				<tr><th>Canchas</th>{ spaces.map( c => <th key={ c.id }>{ c.name }</th> ) }</tr>
			</thead>
			
			<tbody>
			{
				slots.map(( r, i ) => 
					<tr id={i} key={ r.id } className={ r.id < currentSlotId ? "past" : (r.id===currentSlotId ? "current" : "") }>
						<th>{ r.name }</th>
						{
							spaces.map( c => <Cell key={ c.id } tag="td" item={ grid[r.id][c.id] } onClick={ onClick } /> )
						}
					</tr> 
				)
			}
			</tbody>
			
			<tfoot>
				<tr><th>Canchas</th>{ spaces.map( c => <th key={ c.id }>{ c.name }</th> ) }</tr>
			</tfoot>
		</table>
	)
}
export default GridView