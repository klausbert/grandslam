import React, { useContext } from 'react'

import { AppContext } from './AppContext'
import { GridContextProvider } from './GridContext'

import Grid from './Grid'


export default function GridWrapper(routerProps) {
    const appProps = useContext(AppContext)

    return <GridContextProvider {...appProps } {...routerProps }><Grid /></GridContextProvider>
}
