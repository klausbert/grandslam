import React from 'react';
import { ButtonToolbar, ButtonGroup, DropdownButton, MenuItem } from 'react-bootstrap';

const Selector = ({ className, type, types, onUpdate }) => {

	const handleClick = (id) => {
		const type = types.find( f => f.id===id )
		
		onUpdate({ type: type })
	}
	const selectedType = type ? type.id : 0
	const flash = type ? type.flash : false
	
	const tags = [
		{ tag: 'aat',    label: 'AAT' }, 
		{ tag: 'ci',     label: 'Ci Tenis' }, 
		{ tag: 'faccma', label: 'FACCMA' }, 
		{ tag: 'sha',    label: 'SHA' }
	]
	const handleSelect = (tag) => {
		const id = 3
		let type = types.find( f => f.id===id )
		type.tag = tag
		
		onUpdate({ type: type })
	}
	const torneo = type.tag ? tags.find( f => f.tag===type.tag ).label : 'Torneo'

	return (
		<form className={ "navbar-form "+ className }>
			<ButtonToolbar>
				<ButtonGroup>
					<button type="button" id="1" className="btn btn-default" onClick={() => handleClick(1)}>
						<span>Single</span>
					</button>
					<button type="button" id="2" className="btn btn-default" onClick={() => handleClick(2)}>
						<span>Doble</span>
					</button>
				</ButtonGroup>

				<ButtonGroup>
					<DropdownButton pullRight id="3" bsStyle="default" title={ torneo } onClick={() => handleClick(3)}>
					{
						tags.map( m => <MenuItem key={ m.tag } onSelect={ handleSelect } eventKey={ m.tag }>{ m.label }</MenuItem> )
					}
					</DropdownButton>
					<button type="button" id="4" className="btn btn-default" onClick={() => handleClick(4)}>
						<span>Clases</span>
					</button>
					<button type="button" id="5" className="btn btn-default" onClick={() => handleClick(5)}>
						<span>Manten<span className="hidden-xs">imiento</span></span>
					</button>
				</ButtonGroup>
			</ButtonToolbar>
		</form>
	)
}
export default Selector
