import React from 'react';

const Footer = (props) => {
	return (
		<footer className="navbar navbar-default navbar-fixed-bottom">
			<div className="container-fluid">
				<div className="collapse navbar-collapse">
					<div className="row">
						<div className="col-xs-4 col-xs-offset-4 text-center">
							<img className="logo" src="logo-simmons.jpg" alt="sponsor" />
						</div>
						
						<div className="col-xs-4 invisible">
						</div>
					</div>
				</div>
			</div>
		</footer>
	)
}

export default Footer 
