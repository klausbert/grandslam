import React, { Component } from 'react';

import List from './List';
// import List from './List2b';

export default class ListWrapper extends Component {
	state = {
		date: this.props.selectedDate,
		fromTime: "07:00",
		thruTime: "23:00"
	}

	render() {
		const slots = this.props.slots.filter( f => this.state.fromTime <= f.name && f.name <= this.state.thruTime );
		const grid = Object
			.keys(this.props.grid)
			.filter( k => slots.find( f => f.id===parseInt(k, 10) ))
			.reduce((r, c) => { r[c] = this.props.grid[c]; return r }, {});

		console.log(this.state.fromTime, slots, grid)
		return (
			<div>
				<form className="input-group col-xs-4">
					<label className="input-group-addon">Fecha</label>
					<input className="form-control" type="date" name="date"     value={ this.state.date     }                         required             onChange={ this.handleChange } />
					<label className="input-group-addon">Desde</label>
					<input className="form-control" type="time" name="fromTime" value={ this.state.fromTime } min="07:00" max="23:00" required step="1800" onChange={ this.handleChange } />
					<label className="input-group-addon">Hasta</label>
					<input className="form-control" type="time" name="thruTime" value={ this.state.thruTime } min="07:00" max="23:00" required step="1800" onChange={ this.handleChange } />
				</form>
				
				<List {...this.props } grid={ grid } />
			</div>
		)
	}
	//
	handleChange = ({ target }) => {
		if (target) {
			if (target.name==='date') {
				this.props.onUpdate({ selectedDate: target.value })
			}
			else {
				this.setState({[target.name]: target.value })
			}
		}
	}
}
