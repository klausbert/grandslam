import React from 'react';

const Cell = ({ item, onClick }) => { 
	const className = 'type-'+ item.type_id +(item.type_id===2 && item.label==='X' ? '-X' : '')+(item.type_id===3 ? ' '+ item.tag : '') 
	const players = item.players ? item.players.map( m => m.name ).join('/') : null
	const coords = [item.time, 'cancha', item.space_id].join(' ')

	return (
		<td className={ className } title={ players || coords } onClick={() => { onClick(item) }}>
			{ item.type_id <= 3 ? null : item.label }
		</td>
	)
}
export default Cell
