import React, { createContext, PureComponent } from 'react'
import * as api from './api'


export const GridContext = createContext()								//	to be consumed via 16.8's useContext()
export class GridContextProvider extends PureComponent {	
	state = {
		form: { show: false, item: null },
		grid: null,
	}
	render() {
		return (
			<GridContext.Provider value={{ ...this.state, ...this.handles, ...this.props }}>
				{ this.state.grid ? this.props.children : null }
			</GridContext.Provider>
		)
	}
	componentDidMount() {
		this.handles.handleRefresh()
	}	
	componentWillUnmount() {
		clearTimeout(this.refreshTimeout);
	}
	//
	//
	//
	refreshTimeout = null
		
	handles = {
		handleClick: (item) => {
			let cell = Object.assign({}, item)
			
			console.log('Grid.handleClick from Cell', cell, this.props, this.props.dateTime)

			/*
			cell type     | 0 0 F F Q Q Q
			Form selected | 0 1 0 1 0 0 1
			type matches  | - - X X 0 1 X
			--------------+---------------
			open form     | - 1 1 1 - - -
			toggle cell   | 1 - - - - 1 -
			*/
			if (cell.type_id===0) {
				cell.type_id = this.props.type.id;	// adopt the new Type			
				if (this.props.type.form) {
					// var next_id, type_id
					// const dobleSinLugar = (this.props.type.id===2 && 
						// (next_id = this.props.slots.find( f => f.id===cell.slot_id).next_id) && 
						// (type_id = this.state.grid[next_id][cell.space_id].type_id)	//	falla en la última
					// ) 
					// if (dobleSinLugar) {
						// this.handleFlash(true)
					// } else {
						this.setState({ form:{ show: true, item: cell }})	// open <Form />
					// }
				} else {
					if (this.props.type.id===3) {
						cell.tag = this.props.type.tag;	//	logos de torneos
					}
					api.places.post(cell).then( res => {
						console.log('Grid.handleClick cell POST assign', res) 
						this.handles.handleRefresh()
					})
				}
			}
			else {
				if (this.props.types.find( f => f.id===cell.type_id).form) {
					this.setState({ form:{ show: true, item: cell }})	// open <Form />
				}
				else
				if (this.props.type.id===cell.type_id) {
					//	quick resign
					api.places.delete(cell.id).then( res => {
						console.log('Grid.handleClick cell DELETE', res) 
						
						this.props.onUpdate({ show: false, item: {...this.props.item, type_id: 0, players: [] }})
						this.handles.handleRefresh()
					})
				}
				else {
					this.handles.handleFlash(true)
				}
			}
		},
		handleFlash: (flash = false) => {
			this.props.onUpdate({ type: Object.assign({}, this.props.type, { flash: flash }) })
			if (flash)
				setTimeout(() => this.handles.handleFlash(), 1000)
		},
		handleForm: (newState) => {
			console.log('Grid.handleForm new state', newState)
			
			this.setState({ form: newState })	// close the form	// item can be null
			
			this.handles.handleRefresh(true)
		},
		handleRefresh: async (force = false) => {
			if (this.state.form && this.state.form.show && ! force) {
				clearTimeout(this.refreshTimeout);
				return;
			}
			console.log('Grid.handleRefresh', this.props.date)
			const res = await api.places.get(this.props.date)

			let grid = {}
			
			this.props.user &&
			this.props.slots.forEach( r => { 
				grid[r.id] = {}; 
				this.props.spaces.forEach( c => { 
					//	all cells initially "empty"
					grid[r.id][c.id] = { slot_id: r.id, space_id: c.id, type_id: 0, date: this.props.user.today, time: r.name }
				})
			})
			res.map( item => { 
				const label = this.props.types.find( f => f.id===item.type_id ).short
				
				grid[item.slot_id][item.space_id] = {...item, label: label }
				
				if (item.type_id===2) {
					const next_id = this.props.slots.find( f => f.id===item.slot_id).next_id	//	falla en última fila
					
					if (grid[next_id][item.space_id].type_id===0)
						grid[next_id][item.space_id] =  {...item, label: 'X', extended: true }
				}
				return label
			})
			this.setState({ grid })
			
			this.refreshTimeout = setTimeout(() => this.handles.handleRefresh(), 30000)	//	next call
		}	
	}
}
