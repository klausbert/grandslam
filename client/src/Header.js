import React, { useContext } from 'react'
import { Link } from '@reach/router'

import { AppContext } from './AppContext'

import Login from './Login'

import Timer from './Timer'
import DateNav from './DateNav'
import Selector from './Selector'
import Hamburger from './Hamburger'


const ToggleGridListButton = ({ mode, selectedDate }) => {
	const toggle = mode==='grid' ? 'list' : 'grid'
	console.log(mode, 'to', toggle)

	return (
		<Link to={`/${toggle}/${selectedDate}`} className="btn btn-warning">
			<span className={"fa fa-"+(toggle==='grid' ? "list-ol" : "table")}></span>
		</Link>
	)
}


export default function Header({ date: selectedDate, mode }) {
	const { user, showCalendar } = useContext(AppContext)

	return (
		<header id="Header" className="navbar navbar-inverse navbar-fixed-top">
			<div className="container-fluid">
				<div className="navbar-header">
					<div className="text-center">
						<img className="logo img-responsive" src="/Logo-SHA-1926-e1428518742485.png" alt="logo" />
					</div>
				</div>
				
				<div>
					<Timer className="navbar-left" />
					{ user===null ?
						<Login className="navbar-right col-xs-6" /> 
					: user.role==='casillero' ? 
						<div>
							<DateNav   className="navbar-left"  
								selectedDate={ selectedDate } showCalendar={ showCalendar }
							/>
							<Hamburger className="navbar-right" user={ user } />
							{/* <Selector  className={"navbar-right "+(mode==='grid' ? "" : "hidden")}
								type={ props.type } types={ props.types } onUpdate={ props.handleState } 
							/> */}

							<form className="navbar-form navbar-right">
								<div className="btn-group" role="group">
									<ToggleGridListButton mode={ mode } selectedDate={ selectedDate } />
								</div>
							</form>
						</div>
					:
						<div className="navbar-form navbar-right" style={{ margin: '5px 0' }}>
							<img style={{ height: '95px' }} src="/Macabeadas Israel 2017.png" alt="logo" />
						</div>
					}
				</div>
			</div>
		</header>
	)
}
