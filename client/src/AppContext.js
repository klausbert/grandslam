import React, { createContext, useContext, useState, useEffect } from 'react'
import * as api from './api'
import io from "socket.io-client"

const socket = io('http://23.239.0.178:3030')
console.log('socket:', socket)
socket.on('connect', () => { console.log('Socket Connect')});
socket.on('disconnect', () => { console.log('Socket Disconnect')});


export const AppContext = createContext()								//	to be consumed via 16.8's useContext()
export const useStateValue = () => useContext(AppContext)
export const AppContextProvider = ({ children }) => {
	const [lanes, setLanes] = useState([])
	const [slots, setSlots] = useState([])
	const [types, setTypes] = useState([])
	useEffect(() => {
		// socket.on('connect', () => {
			console.log('AppContext emitting')
			socket.emit('find', 'slots',  (error, slots) => (! error && setSlots(slots)))
			socket.emit('find', 'spaces', (error, lanes) => (! error && setLanes(lanes)))
			socket.emit('find', 'types',  (error, types) => (! error && setTypes(types)))
		// })
	}, [])
	const [user, setUser] = useState(sessionStorage.user ? JSON.parse(sessionStorage.user) : null)
	const handleLogin = (user, pass) => {
		api.login({ user, pass })
		.then( user => {
			if (! user.error) {
				setUser(user)
				console.log('handleLogin', user);
				sessionStorage.user = JSON.stringify(user);
			} else {
				setUser(null)
				sessionStorage.user = null;				
			}
		})
	}
	console.log('AppContext', lanes.length, slots.length, types.length, user)
	
	if (lanes.length * slots.length * types.length===0) return null

	return (
		<AppContext.Provider value={{ slots, spaces: lanes, types, user, handleLogin, handleState: ()=>{} }}>
			{ children }
		</AppContext.Provider>
	)
}
