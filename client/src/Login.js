import React, { useRef, useContext } from 'react'
import { AppContext } from './AppContext'

export default function Login({ className }) {
	const user = useRef('')
	const pass = useRef('')
	const { handleLogin } = useContext(AppContext)
	const handleClick = () => handleLogin(user.current.value, pass.current.value)
	return (
		<form id="Login" className={ "navbar-form "+ className }>
			<div className="input-group">
				<label className="input-group-addon"><span className="fa fa-user"></span></label>
				<input className="form-control" type="text"     placeholder="user" ref={ user } required />
				
				<label className="input-group-addon"><span className="fa fa-key"></span></label>
				<input className="form-control" type="password" placeholder="pass" ref={ pass } required />
				
				<div className="input-group-btn">
					<button className="btn" type="button" onClick={ handleClick }><span className="fa fa-sign-in"></span></button>
				</div>
			</div>
		</form>
	)
}
