const db = require('../../common/db.js')

/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};
  }

  setup (app) {
    this.app = app
  }

  async find (params) {
    console.log(params)
    if (params.query.date) return await db.query(`
      SELECT B.*
      , GROUP_CONCAT(P.id        SEPARATOR ',') AS player_ids
      , GROUP_CONCAT(P.member_id SEPARATOR ',') AS member_ids
      FROM places B
      JOIN players P ON B.id = P.place_id
      WHERE date LIKE '${params.query.date}%'
      GROUP BY B.id
    `);

    return await db.query(`
      SELECT B.*
      , GROUP_CONCAT(P.id        SEPARATOR ',') AS player_ids
      , GROUP_CONCAT(P.member_id SEPARATOR ',') AS member_ids
      FROM places B
      JOIN players P ON B.id = P.place_id
      GROUP BY B.id
      ORDER BY B.id DESC LIMIT 50
    `);
  }

  async get (id, params) {
    return  await db.query(`
      SELECT B.*
      , GROUP_CONCAT(P.id        SEPARATOR ',') AS player_ids
      , GROUP_CONCAT(P.member_id SEPARATOR ',') AS member_ids
      FROM places B
      JOIN players P ON B.id = P.place_id
      WHERE B.id = ${id}
  `);
  }

  async create (data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return await db.query(`INSERT INTO places SET ?`, data);
    // {
    //   "fieldCount": 0,
    //   "affectedRows": 1,
    //   "insertId": 61702,
    //   "info": "",
    //   "serverStatus": 2,
    //   "warningStatus": 0
    // }
  }

  async update (id, data, params) {
    const players = this.app.service('players')

    return players.update(id, data)
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
