// Initializes the `types` service on path `/types`
const createService = require('./types.class.js');
const hooks = require('./types.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/types', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('types');

  service.hooks(hooks);
};
