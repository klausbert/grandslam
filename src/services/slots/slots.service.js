// Initializes the `slots` service on path `/slots`
const createService = require('./slots.class.js');
const hooks = require('./slots.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/slots', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('slots');

  service.hooks(hooks);
};
