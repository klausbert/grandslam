const { BadRequest } = require('@feathersjs/errors');


const validate = async ({ type, method, path, data }) => {
  console.log('Validate', type, method, path, data)

  const requiredFields = {
    update: "member_ids"
  }[method].split(',')

  if (requiredFields.find( k => data[k]===undefined )) {
    throw new BadRequest('Missing args')
  }
}


module.exports = {
  before: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [validate],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
