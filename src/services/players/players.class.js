const db = require('../../common/db.js')

/* eslint-disable no-unused-vars */
class Service {
  constructor (options) {
    this.options = options || {};
  }

  async find (params) {
    if (params.query.ids) return await db.query(`
      SELECT * FROM players P 
      JOIN members M ON P.member_id = M.id
      WHERE P.id IN (${params.query.ids})
    `);
    if (params.query.place_id) return await db.query(`
      SELECT * FROM players P 
      JOIN members M ON P.member_id = M.id
      WHERE place_id = ${params.query.place_id}
    `);
    // else
    return [];
  }

  async get (id, params) {
    return await db.query(`
      SELECT * FROM players P 
      JOIN members M ON P.member_id = M.id
      WHERE P.id = ${id}
    `);
  }

  async create (data, params) {
    if (Array.isArray(data)) {
      return Promise.all(data.map(current => this.create(current, params)));
    }

    return data;
  }

  async update (place_id, data, params) {
    // if (data.member_ids) {
      const prev = await db.query(`
        SELECT member_id FROM players WHERE place_id = ${place_id}
      `).then( data => data.map( m => parseInt(m.member_id, 10)))

      const curr = data.member_ids.split(',').map( m => parseInt(m, 10))

      const drop = prev.filter( f => ! curr.some( s => s===f ))
      const next = curr.filter( f => ! prev.some( s => s===f ))

      drop.length && await db.query(`
        DELETE FROM players WHERE place_id = ${place_id} AND member_id IN (${drop.join(',')})
      `)
      return Promise.all(next.map( member_id => db.query(`
        INSERT INTO players SET ?
      `, { place_id, member_id })))
    // }
    // return data;
  }

  async patch (id, data, params) {
    return data;
  }

  async remove (id, params) {
    return { id };
  }
}

module.exports = function (options) {
  return new Service(options);
};

module.exports.Service = Service;
