// Initializes the `spaces` service on path `/spaces`
const createService = require('./spaces.class.js');
const hooks = require('./spaces.hooks');

module.exports = function (app) {
  
  const paginate = app.get('paginate');

  const options = {
    paginate
  };

  // Initialize our service with any options it requires
  app.use('/spaces', createService(options));

  // Get our initialized service so that we can register hooks
  const service = app.service('spaces');

  service.hooks(hooks);
};
