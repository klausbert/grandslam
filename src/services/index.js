const slots = require('./slots/slots.service.js');
const spaces = require('./spaces/spaces.service.js');
const types = require('./types/types.service.js');
const places = require('./places/places.service.js');
const players = require('./players/players.service.js');
const members = require('./members/members.service.js');
const users = require('./users/users.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(slots);
  app.configure(spaces);
  app.configure(types);
  app.configure(places);
  app.configure(players);
  app.configure(members);
  app.configure(users);
};
