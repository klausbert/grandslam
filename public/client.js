// Create a websocket connecting to our Feathers server
const socket = io('http://23.239.0.178:3030');
// Create a Feathers application
const app = feathers();
// Configure Socket.io client services to use that socket
app.configure(feathers.socketio(socket));

app.service('authenticated').on('created', message => {
  console.log('Someone created a message', message);
});

async function createAndList() {
  await app.service('authenticated').create({
    text: 'Hello from Feathers browser client'
  });

  const authenticated = await app.service('authenticated').find();

  console.log('authenticated', authenticated);
}

createAndList();
